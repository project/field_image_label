INTRODUCTION
------------
Field Image Label module allows to use different field label for displaying
fields from the label used when viewing the field in a form.

INSTALLATION
------------
1. Download and unpack the Field Image Label module directory in your modules
   folder  (this is usually "sites/all/modules/"). See
   https://drupal.org/documentation/install/modules-themes/modules-7 for
   addition information.
2. Enable module.

CONFIGURATION
-------------

Steps to make field label as Image.

 1. Go to Administration/Structure/Contenttypes/YOUR CONTENT TYPE/Manage fields.
 2. Edit any field.
 3. Choose image in "Display Image" with displaying label.

CUSTOMIZATION
-------------

After installation of this module creates a image style namely 
"Field Image Label" which you can edit from below menu

Path Home » Administration » Configuration » Media » Image styles
URL: admin/config/media/image-styles/edit/field_image_label

-- CONTACT --

Current maintainers:
* Rajveer singh https://www.drupal.org/u/rajveergang
  email : rajveer.gang@gmail.com
