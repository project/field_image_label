<?php

/**
 * @file
 * Main file for Field Display Label module.
 */

/**
 * Implements hook_form_FORM_ID_alter().
 */
function field_image_label_form_field_ui_field_edit_form_alter(&$form, $form_state, $form_id) {
  if (!isset($form['instance'])) {
    return;
  }
  $form['instance']['display_image_label'] = array(
    '#type' => 'managed_file',
    '#title' => t('Display Image'),
    '#description' => t('A separate image label for viewing this field. Leave blank to use the default field label.'),
    '#default_value' => isset($form['#instance']['display_image_label']) ? $form['#instance']['display_image_label'] : '',
    '#upload_validators' => array(
      'file_validate_extensions' => array('gif png jpg jpeg'),
    ),
    '#upload_location' => 'public://field_image_label_display_image_label/',
    '#weight' => $form['instance']['label']['#weight'] + 1,
  );
}

/**
 * Implements hook_form_alter().
 */
function field_image_label_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == 'field_ui_field_edit_form') {
    $form['#submit'][] = 'field_image_label_custom_submit';
  }
}

/**
 * Implements hook_form_submit().
 */
function field_image_label_custom_submit($form, &$form_state) {
  $display_image_label = $form_state['values']['instance']['display_image_label'];
  if (!empty($display_image_label)) {
    $file = file_load($display_image_label);
    $file->status = 1;
    file_save($file);
  }
}

/**
 * Implements hook_preprocess_field().
 */
function field_image_label_preprocess_field(&$variables) {
  $field = field_info_instance($variables['element']['#entity_type'], $variables['element']['#field_name'], $variables['element']['#bundle']);
  if (isset($field['display_image_label']) && strlen(trim($field['display_image_label'])) > 0 && $field['display_image_label'] != 0) {
    if (module_exists('i18n_field')) {
      $field_image_object = file_load(check_plain(i18n_field_translate_property($field, 'display_image_label')));
      $variables['label'] = field_image_label_get_image($field_image_object);
    }
    else {
      $field_image_object = file_load(check_plain($field['display_image_label']));
      $variables['label'] = field_image_label_get_image($field_image_object);
    }
  }
}

/**
 * Implements hook_i18n_object_info().
 */
function field_image_label_i18n_object_info_alter(&$info) {
  if (!isset($info['field_instance'])) {
    return;
  }
  $info['field_instance']['string translation']['properties']['display_image_label'] = t('Display label');
}

/**
 * Function returns image w.r.t. Image  object.
 *
 * @param object $image_object
 *   Image object to return field label.
 *
 * @return string
 *   Returns image for field label.
 */
function field_image_label_get_image(stdClass $image_object) {
  if (!empty(image_style_load('field_image_label')) && is_object($image_object)) {
    $image = theme('image_style', array('path' => $image_object->uri, 'style_name' => 'field_image_label'));
  }
  else {
    $image = "<img src='" . file_create_url($image_object->uri) . "'>";
  }
  return $image;
}
